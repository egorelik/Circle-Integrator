package sample;

public class Mask {

	private Matrix matrix;
	private double maxLimit;
	private int[] maskedX;
	private int[] maskedY;
	private int maskedAmount;
	
	public Mask() {}
	
	public Mask(double maxLimit, Matrix matrix) {
		this.maxLimit = maxLimit;
		this.matrix = matrix;
		maskedX =  new int[matrix.numCols()*matrix.numRows()];
		maskedY = new int[matrix.numCols()*matrix.numRows()];
	}
	
	public Matrix getMaskedMatrix() {
		short[] matData = matrix.getData();
		Matrix newMat = new Matrix(matrix.numRows(), matrix.numCols());
		int masked = 0;
		int x, y;
		for(int i = 0; i < matData.length; i++) {
			x = i % matrix.numCols();
			y = i / matrix.numRows();
			if(matData[i] > (short) maxLimit) {
				masked++;
				maskedX[masked] = x;
				maskedY[masked] = y;
				newMat.set(x, y, Short.MIN_VALUE) ;
			} else {
				newMat.set(x, y, matData[i]);
			}
		}
		maskedAmount = masked;
		return newMat;
	}
	public int[] getMaskedX() {
		return maskedX;
	}
	public int[] getMaskedY() {
		return maskedY;
	}
	public int maskedAmount() {
		return maskedAmount;
	}
	public void changeMaxLimit(int value) {
		maxLimit = value;
	}
}
