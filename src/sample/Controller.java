package sample;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.canvas.Canvas;

import sample.centerFindTool.CenterFindTool;
import sample.interpolation.InterpolationBox;
import sample.interpolation.Interpolator;
import sample.log.Log;
import sample.point.DoublePoint;
import sample.point.IntegerPoint;
import sample.point.SynchronizedList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Controller {
	@FXML
	public CanvasWithZoom canvasWithZoom;

	public Label xCenterCoordinateLabel;
	public Label yCenterCoordinateLabel;
	public Label xInterpolatedCenterCoordinateLabel;
	public Label yInterpolatedCenterCoordinateLabel;

	public Label maxValueOfMatrixLabel;
	public Label minValueOfMatrixLabel;
	public Label meanLabel;
	public Label sizeLabel;

	public Label maxValueOfMaskMatrixLabel;
	public Label minValueOfMaskMatrixLabel;
	public Label meanMaskLabel;

	public LineChart chart;

	public TextArea console;
	private PrintStream ps;

	public TextField maskOpt;

	private DoublePoint centerOfCircle;
	private DoublePoint interpolatedCenter;
	private Matrix matrix;
	private CenterFindTool centerFindTool;
	private double[] mapRadiusToIntegratedValue;

	public void initialize() {
		Registry.setCanvasWithZoom(canvasWithZoom);
		canvasWithZoom.setMinCanvasWidth(512);
		canvasWithZoom.setMinCanvasHeight(512);
		chart.setCreateSymbols(false);
		ps = new PrintStream(new Console(console));
		console.setEditable(false);
	}
	

	public void onFileChooserButtonClick(ActionEvent actionEvent) {
		FileChooser fileChooser = new FileChooser();

		fileChooser.setInitialDirectory(new File(Log.lastDir));
		fileChooser.setTitle("Open Resource File");
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("tif", "*.tif"));
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("dm3", "*.dm3"));
		File file = fileChooser.showOpenDialog(new Stage());
		if (file != null) {
			try {
				Log.log(file.getParentFile().getAbsolutePath());
			} catch (IOException e1) {
			}

			if (file.getName().substring(file.getName().lastIndexOf(".") + 1).toLowerCase().equals("tif")) {
				TiffImageReader tiffImageReader = new TiffImageReader();
				matrix = tiffImageReader.readImage(file);
			} else if (file.getName().substring(file.getName().lastIndexOf(".") + 1).toLowerCase().equals("dm3")) {
				DM3_Reader dm3ImageReader = new DM3_Reader();
				matrix = dm3ImageReader.readImage(file);
			} else {
				console.appendText("No accepted Format.\n");
				return;
			}

			WritableImage writableImage = Plotter.getFXColoredImage(matrix);
			canvasWithZoom.setAndShowImage(writableImage);

			if (centerFindTool != null) {
				canvasWithZoom.getContentGroup().getChildren().remove(centerFindTool);
				canvasWithZoom.clearInterpolationBoxes();
			}

			minValueOfMatrixLabel.setText(String.format("%.1f", (double) Statistic.getMinValue(matrix)));
			maxValueOfMatrixLabel.setText(String.format("%.1f", (double) Statistic.getMaxValue(matrix)));

			meanLabel.setText(String.format("%.1f", Statistic.mean(matrix)));
			sizeLabel.setText("x:" + matrix.numCols() + " y:" + matrix.numRows());

			console.appendText(file.getName() + " with dimensions x:" + matrix.numCols() + " y:" + matrix.numRows() + " loaded.\n"
					+ "Image size is " + file.length() + " bytes.\n");

		}
	}

	private void integrateIntensities() {
		Thread thread = new Thread() {
			@Override
			public void run() {
				int width = matrix.numCols();
				int height = matrix.numRows();

				int maxRadius = getMaxRadius();
				SynchronizedList<IntegerPoint>[] mapRadiusToPoints = new SynchronizedList[maxRadius];
				for (int i = 0; i < mapRadiusToPoints.length; i++) {
					int size = (int) (10 * i) + 1;
					mapRadiusToPoints[i] = new SynchronizedList<>(size);
				}
				int cores = Runtime.getRuntime().availableProcessors();
				int halfWidth = width / 2;
				ExecutorService taskExecutor = Executors.newFixedThreadPool(cores);
				for (int x = 0; x < halfWidth; x++) {
					taskExecutor.execute(createThreadForRadiusCalculation(x, height, mapRadiusToPoints));
					taskExecutor.execute(createThreadForRadiusCalculation(x + halfWidth, height, mapRadiusToPoints));
				}
				taskExecutor.shutdown();
				try {
					taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
				} catch (InterruptedException e) {
					System.out.println(e);
				}

				mapRadiusToIntegratedValue = new double[maxRadius];
				taskExecutor = Executors.newFixedThreadPool(cores);
				for (int radius = 0; radius < maxRadius; radius++) {
					taskExecutor.execute(createThreadForRadiusIntegration(radius, mapRadiusToPoints));
				}
				taskExecutor.shutdown();
				try {
					taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
				} catch (InterruptedException e) {
					System.out.println(e);
				}
				Runnable runnable = new Runnable() {
					@Override
					public void run() {
						showChart();
					}
				};
				Platform.runLater(runnable);
			}
		};
		thread.start();
	}

	public void onIntegrateValueButtonClick(ActionEvent actionEvent) {
		if (matrix == null) {
			console.appendText("No file selected.\n");
			return;
		}
		if (canvasWithZoom.getContentGroup().getChildren().contains(centerFindTool))
			return;

		System.out.println(Registry.getCanvasWithZoom().getImageHeight());
		System.out.println(canvasWithZoom.getImageHeight());
		centerFindTool = new CenterFindTool();
		centerFindTool.addOnCenterChangeListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observableValue, Object o, Object o2) {
				centerOfCircle = (DoublePoint) o2;

				xCenterCoordinateLabel.setText(String.format("%.1f", centerOfCircle.getX()));
				yCenterCoordinateLabel.setText(String.format("%.1f", centerOfCircle.getY()));
				interpolateCenter();
			}
		});
		centerFindTool.addOnChangeEndListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observableValue, Object o, Object o2) {
				integrateIntensities();
			}
		});
		canvasWithZoom.addTool(centerFindTool);
	}

	public void setView(Matrix m) {
		WritableImage writableImage = Plotter.getFXColoredImage(m);
		canvasWithZoom.setAndShowImage(writableImage);
	}

	ClickMask cm;

	public void onSelectPointsButtonClick(ActionEvent actionEvent) {
		if (matrix == null) {
			console.appendText("No file selected.\n");
			return;
		}
		if (cm != null)
			canvasWithZoom = cm.getCanvas();
		console.appendText("Click left mouse button to set point.\n" + "Click right mouse button to create Polygon.\n");
		cm = new ClickMask(canvasWithZoom, matrix);
		canvasWithZoom = cm.addEvent();
	}

	public void onApplyMaskButtonClick(ActionEvent actionEvent) {
		if (matrix == null) {
			console.appendText("No file selected.\n");
			maskOpt.setText("");
			return;
		}
		try { // Try apllieng mask value
			if (!maskOpt.getText().isEmpty()) {
				double maskVal = Double.parseDouble(maskOpt.getText());
				Mask mask = new Mask(maskVal, matrix);
				Matrix maskedMatrix = mask.getMaskedMatrix();

				console.appendText(mask.maskedAmount() + " Pixels masked by value.\n");

				maskOpt.setText("");

				setView(maskedMatrix);
				meanMaskLabel.setText(String.format("%.1f", Statistic.mean(maskedMatrix)));

				maxValueOfMaskMatrixLabel.setText(String.format("%.1f", (double) Statistic.getMaxValue(maskedMatrix)));
				minValueOfMaskMatrixLabel.setText(String.format("%.1f", (double) Statistic.getMinValue(maskedMatrix)));

				return;
			}
		} catch (Exception e) {
			console.appendText("No usable value.\n");
			maskOpt.setText("");
		}
		try { // try creating mask from view
			canvasWithZoom = cm.getCanvas();
			setView(cm.getMaskedMatrix());

			meanMaskLabel.setText(String.format("%.1f", Statistic.mean(cm.getMaskedMatrix())));
			maxValueOfMaskMatrixLabel
					.setText(String.format("%.1f", (double) Statistic.getMaxValue(cm.getMaskedMatrix())));
			minValueOfMaskMatrixLabel
					.setText(String.format("%.1f", (double) Statistic.getMinValue(cm.getMaskedMatrix())));
			console.appendText(cm.maskedAmount() + " Pixels masked by drawing.\n");
			cm = null;
		} catch (Exception e) {
		}
	}

	public void onShowOriginalButtonClick(ActionEvent actionEvent) {
		if (matrix == null) {
			console.appendText("No file selected.\n");
			return;
		}
		if (cm != null)
			canvasWithZoom = cm.getCanvas();

		meanMaskLabel.setText("");
		minValueOfMaskMatrixLabel.setText("");
		maxValueOfMaskMatrixLabel.setText("");

		setView(matrix);
	}

	public void onExportButtonClick() {
		if (matrix == null)
			return;
		if (chart.getData().get(0) == null)
			return;
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File(Log.lastDir));
		fileChooser.setTitle("Export Integrated Values");
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text", ".txt"));
		File outputFile = fileChooser.showSaveDialog(new Stage());
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "utf-8"))) {
			try {
				Log.log(outputFile.getParentFile().getAbsolutePath());
			} catch (IOException e1) {
			}
			XYChart.Series data = (XYChart.Series) chart.getData().get(0);
			XYChart.Data val;
			for (Object d : data.getData()) {
				val = (XYChart.Data) d;
				writer.write("(" + val.getXValue() + ";" + val.getYValue() + ")\r");
			}
		} catch (Exception e) {
		}
	}

	private int getMaxRadius() {
		int width = matrix.numCols();
		int height = matrix.numRows();
		IntegerPoint top_left = new IntegerPoint(0, 0);
		IntegerPoint top_right = new IntegerPoint(width, 0);
		IntegerPoint bottom_left = new IntegerPoint(0, height);
		IntegerPoint bottom_right = new IntegerPoint(width, height);
		double distance1 = interpolatedCenter.distanceToPoint(top_left);
		double distance2 = interpolatedCenter.distanceToPoint(top_right);
		double distance3 = interpolatedCenter.distanceToPoint(bottom_left);
		double distance4 = interpolatedCenter.distanceToPoint(bottom_right);

		double max1 = Math.max(distance1, distance2);
		double max2 = Math.max(distance3, distance4);
		return (int) Math.max(max1, max2) + 2;
	}

	private Runnable createThreadForRadiusIntegration(int radius, SynchronizedList<IntegerPoint>[] mapRadiusToPoints) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				integrateRadiusPoints(radius, mapRadiusToPoints);
			}
		};
		return runnable;
	}

	private void integrateRadiusPoints(int radius, SynchronizedList<IntegerPoint>[] mapRadiusToPoints) {
		SynchronizedList<IntegerPoint> pointsInRadius = mapRadiusToPoints[radius];

		if (!pointsInRadius.isEmpty()) {
			double integratedValue = 0;
			for (int i = 0; i < pointsInRadius.size(); i++) {
				IntegerPoint point = pointsInRadius.get(i);
				integratedValue = integratedValue + point.getValue();
			}
			double averageIntegratedValue = integratedValue / pointsInRadius.size();
			mapRadiusToIntegratedValue[radius] = averageIntegratedValue;
		}
	}

	private Runnable createThreadForRadiusCalculation(int x, int height,
			SynchronizedList<IntegerPoint>[] mapRadiusToPoints) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				calculateCirclePoints(x, height, mapRadiusToPoints);
			}
		};
		return runnable;
	}

	private void calculateCirclePoints(int x, int height, SynchronizedList<IntegerPoint>[] mapRadiusToPoints) {
		for (int y = 0; y < height; y++) {
			int value = matrix.get(x, y);
			IntegerPoint point = new IntegerPoint(x, y);
			point.setValue(value);
			int radius = (int) Math.round(interpolatedCenter.distanceToPoint(point));
			addRadiusPoint(radius, point, mapRadiusToPoints);
		}
	}

	private void addRadiusPoint(int radius, IntegerPoint point, SynchronizedList<IntegerPoint>[] mapRadiusToPoints) {
		// if(radius >= mapRadiusToPoints.length){
		// System.out.println(radius + ", " + mapRadiusToPoints.length);
		// }
		SynchronizedList<IntegerPoint> points = mapRadiusToPoints[radius];
		points.add(point);
	}

	private void showChart() {
		XYChart.Series integratedValuesChartData = new XYChart.Series();
		integratedValuesChartData.setName("Integrated Values");

		for (int radius = 1; radius < mapRadiusToIntegratedValue.length; radius++) {
			double integratedValue = mapRadiusToIntegratedValue[radius];
			integratedValuesChartData.getData().add(new XYChart.Data(radius, integratedValue));
		}
		chart.getData().clear();
		chart.getData().add(integratedValuesChartData);
	}

	private void interpolateCenter() {
		Interpolator interpolator = new Interpolator();
		interpolator.setCircleCenterFromUser(centerOfCircle);
		interpolator.setMatrix(matrix);
		interpolator.setCircleRadiusFromUser(centerFindTool.getCircleRadius());
		interpolator.interpolate();
		interpolatedCenter = interpolator.getInterpolatedCenter();
		xInterpolatedCenterCoordinateLabel.setText(String.format("%.1f", interpolatedCenter.getX()));
		yInterpolatedCenterCoordinateLabel.setText(String.format("%.1f", interpolatedCenter.getY()));
		LinkedList<InterpolationBox> boxes = interpolator.getInterpolationBoxes();
		canvasWithZoom.showInterpolateBoxes(boxes);
	}
}
