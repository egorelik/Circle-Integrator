package sample.log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Log {

	public static File logFile;
	public static String lastDir;
	
	public static void log(String log) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));
		writer.write(log);
		writer.close();
		lastDir = log;
	}
	
	public static void loadExisting(File log) throws FileNotFoundException {
		if(log == null)return;
		logFile = log;
		BufferedReader reader = new BufferedReader(new FileReader(log));
		try {
			String ln;
			while((ln = reader.readLine())!=null) lastDir = ln;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void init() throws FileNotFoundException {
		String home = System.getProperty("user.home");
		File f = new File(home+"\\.circleintegrator\\settings.log");
		if(f.exists()) loadExisting(f);
		else {
			try {
				f.getParentFile().mkdirs();
				f.createNewFile();
				logFile = f;
				lastDir = home;
				log(lastDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
