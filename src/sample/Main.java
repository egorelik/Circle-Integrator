package sample;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;
import sample.log.Log;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("mainControl.fxml"));

		Screen screen = Screen.getPrimary();
		Rectangle2D bounds = screen.getVisualBounds();

		primaryStage.setWidth(bounds.getWidth());
		primaryStage.setHeight(bounds.getHeight());
		
		Log.init();

		double minW = bounds.getMinX(), minH = bounds.getMinY();
		
		primaryStage.setX(minW);
		primaryStage.setY(minH);
		
//		primaryStage.setFullScreen(true);
		primaryStage.setTitle("Circle Integrator");
		primaryStage.setMinWidth(minW);
		primaryStage.setMinHeight(minH);
		Scene scene = new Scene(root, minW, minH);		

//		root.scaleXProperty().bind(scene.widthProperty().divide(bounds.getWidth()));
//		root.scaleYProperty().bind(scene.heightProperty().divide(bounds.getHeight()));

		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
