package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.Optional;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import sample.log.Log;
import sample.mosaic.Mosaic;
import sample.point.DoublePoint;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Color;

public class MosaicController {
	public ScrollPane imageView;
	public Canvas view;

	private LinkedList<Matrix> matrices = new LinkedList<Matrix>();
	private LinkedList<DoublePoint> centers = new LinkedList<DoublePoint>();
	private Matrix result;

	public TextArea console;
	private PrintStream ps;

	public void initialize() {
		ps = new PrintStream(new Console(console));
		console.setEditable(false);
	}

	public void onAddFileButtonClick(ActionEvent actionEvent) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File(Log.lastDir));
		fileChooser.setTitle("Open Resource File");
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("tif", "*.tif"));
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("dm3", "*.dm3"));
		File file = fileChooser.showOpenDialog(new Stage());
		if (file != null) {

			try {
				Log.log(file.getParentFile().getAbsolutePath());
			} catch (IOException e1) {
			}

			TextInputDialog dialog = new TextInputDialog();
			dialog.setTitle("Center Coordinates");
			dialog.setHeaderText("Enter Coordinates of Center.");
			Label xLabel = new Label("X Coordinate: ");
			Label yLabel = new Label("Y Coordinate: ");
			TextField xCoor = new TextField();
			TextField yCoor = new TextField();
			GridPane grid = new GridPane();
			grid.add(xLabel, 1, 1);
			grid.add(yLabel, 2, 1);
			grid.add(xCoor, 1, 2);
			grid.add(yCoor, 2, 2);
			dialog.getDialogPane().setContent(grid);
			Optional<String> result = dialog.showAndWait();

			if (!result.isPresent())
				return;
			try {
				centers.add(new DoublePoint(Double.parseDouble(xCoor.getText()), Double.parseDouble(yCoor.getText())));
			} catch (Exception e) {
				console.appendText("Wrong entry. No File Loaded.\n");
				return;
			}
			if (file.getName().substring(file.getName().lastIndexOf(".") + 1).toLowerCase().equals("tif")) {
				TiffImageReader tiffImageReader = new TiffImageReader();
				matrices.add(tiffImageReader.readImage(file));
			} else if (file.getName().substring(file.getName().lastIndexOf(".") + 1).toLowerCase().equals("dm3")) {
				DM3_Reader dm3ImageReader = new DM3_Reader();
				matrices.add(dm3ImageReader.readImage(file));
			} else {
				console.appendText("No accepted Format.\n");
				return;
			}

			console.appendText(
					file.getName() + " added. Center Coordinates (" + xCoor.getText() + ";" + yCoor.getText() + ")\n");
		}
	}

	private boolean drawed = false;

	public void drawDiagonal(ActionEvent e) {
		if (!drawed) {
			view.getGraphicsContext2D().setStroke(Color.YELLOW);
			view.getGraphicsContext2D().strokeLine(0, 0, view.getHeight(), view.getWidth());
			view.getGraphicsContext2D().strokeLine(0, view.getHeight(), view.getWidth(), 0);
			drawed = true;
			return;
		}
		drawed = false;
		view.getGraphicsContext2D().clearRect(0, 0, view.getWidth(), view.getHeight());
		if (result != null) {
			view.getGraphicsContext2D().drawImage(Plotter.getFXColoredImage(result), 0, 0, view.getWidth(),
					view.getHeight());
		}
	}

	public void onExportImageClick() {
		if (result == null) {
			console.appendText("No results to export. Please create mosaic.");
		}
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File(Log.lastDir));
		fileChooser.setTitle("Save Image File");
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("png", ".png"));
		File outputFile = fileChooser.showSaveDialog(new Stage());
		BufferedImage bImage = SwingFXUtils.fromFXImage(Plotter.getFXColoredImage(result), null);
		try {
			Log.log(outputFile.getParentFile().getAbsolutePath());
			ImageIO.write(bImage, "png", outputFile);
		} catch (IOException e) {}
	}

	public void onShowImageButtonClick(ActionEvent e) {
		if (matrices.size() < 1)
			return;
		Mosaic m = new Mosaic(matrices, centers);
		result = m.getMosaic();
		view.getGraphicsContext2D().drawImage(Plotter.getFXColoredImage(result), 0, 0, view.getWidth(),
				view.getHeight());
	}
	
	public void onClearMosaicClick(ActionEvent e) {
		matrices.removeAll(matrices);
		centers.removeAll(centers);
		result = null;
		view.getGraphicsContext2D().clearRect(0, 0, view.getWidth(), view.getHeight());
		console.appendText("Matrix list cleared.");
	}
}
