package sample;

import javafx.scene.layout.AnchorPane;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.Tab;
import javafx.scene.Parent;

import java.io.IOException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.fxml.FXML;

public class MainController {

	@FXML
	private MenuBar menu;
	@FXML
	private TabPane tabs;
	
	public void initialize() {
		tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
	}

	public TabPane getTabs() {
		return tabs;
	}

	public MenuBar getMenu() {
		return menu;
	}
}
