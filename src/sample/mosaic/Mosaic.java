package sample.mosaic;

import java.util.LinkedList;

import sample.Matrix;
import sample.point.DoublePoint;

public class Mosaic {

	LinkedList<Matrix> matrices;
	LinkedList<DoublePoint> centers;

	public Mosaic(LinkedList<Matrix> m, LinkedList<DoublePoint> c) {
		matrices = m;
		centers = c;
	}

	public Matrix getMosaic() {
		int size[] = determineSize();
		Matrix mosaic = new Matrix(size[0] * 2, size[1] * 2);
		for (int y = 0; y < size[1] * 2; y++) {
			for (int x = 0; x < size[0] * 2; x++) {
				mosaic.set(x, y, Short.MIN_VALUE);
			}
		}
		int xVal, yVal;
		for (int i = 0; i < matrices.size(); i++) {
			for (int y = 0; y < matrices.get(i).numRows(); y++) {
				for (int x = 0; x < matrices.get(i).numCols(); x++) {
					xVal = (int) (x - centers.get(i).getX() + size[0]);
					yVal = (int) (y - centers.get(i).getY() + size[1]);
					try {
						mosaic.set(xVal, yVal, matrices.get(i).get(x, y));
					} catch (Exception e) {}
				}
			}
		}
		return mosaic;
	}

	private int[] determineSize() {
		double width = 0, hight = 0, temp;
		for (int i = 0; i < matrices.size(); i++) {
			temp = Math.max(Math.abs(matrices.get(i).numCols() - centers.get(i).getX()),centers.get(i).getX());
			if (width < temp)
				width = temp;
			temp = Math.max(Math.abs(matrices.get(i).numRows() - centers.get(i).getY()),centers.get(i).getY());
			if (hight < temp)
				hight = temp;
		}
		return new int[] { (int) width, (int) hight };
	}
}
