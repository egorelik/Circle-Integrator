package sample;

import java.util.LinkedList;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import sample.point.DoublePoint;
import javafx.scene.shape.Polygon;

public class ClickMask extends Mask {

	private CanvasWithZoom canvasWithZoom;

	private Matrix matrix;
	private EventHandler<MouseEvent> eh;

	private double rectSize = 3;

	private int maskedAmount = 0;

	private LinkedList<Rectangle> rect;
	private Polygon p;

	public ClickMask(CanvasWithZoom canvasWithZoom, Matrix matrix) {
		this.canvasWithZoom = canvasWithZoom;
		this.matrix = matrix;
		rect = new LinkedList<Rectangle>();
		eh = new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				if (e.getButton() == MouseButton.PRIMARY) {
					Rectangle r = new Rectangle(0, 0, rectSize, rectSize);
					r.setTranslateX(e.getX());
					r.setTranslateY(e.getY());
					rect.add(r);
					canvasWithZoom.getContentGroup().getChildren().add(rect.getLast());
				}
				if (e.getButton() == MouseButton.SECONDARY) {
					if (rect.size() < 3) {
						return;
					}
					canvasWithZoom.removeEventFilter(MouseEvent.MOUSE_CLICKED, eh);
					Double[] coord = new Double[rect.size() * 2];
					for (int i = 0; i < rect.size(); i++) {
						coord[2 * i] = rect.get(i).getTranslateX();
						coord[2 * i + 1] = rect.get(i).getTranslateY();
					}

					for (Rectangle r : rect)
						canvasWithZoom.getContentGroup().getChildren().remove(r);
					p = new Polygon();
					p.getPoints().addAll(coord);
					canvasWithZoom.getContentGroup().getChildren().add(p);
				}
			}
		};
	}

	private LinkedList<DoublePoint> toPoints(double w, double h) {
		LinkedList<DoublePoint> points = new LinkedList<DoublePoint>();
		for (int i = 0; i < p.getPoints().size() - 1; i += 2) {
			points.add(new DoublePoint(p.getPoints().get(i) * w ,p.getPoints().get(i + 1) * h));
		}
		return points;
	}
	

	public Matrix getMaskedMatrix() {

		double hRate = matrix.numRows() / canvasWithZoom.getContentHeight();
		double wRate = matrix.numCols() / canvasWithZoom.getContentWidth() ;
		
		Matrix scaled = new Matrix((int) matrix.numCols(), (int) matrix.numRows());
		
		Double[] points = new Double[p.getPoints().size()];
		
		int i = -1;
		for (DoublePoint d : toPoints(wRate, hRate)) {
			points[++i] = d.getX();
			points[++i] = d.getY();
		}

		p.getPoints().setAll(points);
		
		for (int y = 0; y < matrix.numRows(); y++) {
			for (int x = 0; x < matrix.numCols(); x++) {
				//mark points between polygon points
				if (p.contains(x, y)) {
					scaled.set(x, y, Short.MIN_VALUE);
					maskedAmount++;
				}
				else {
					scaled.set(x, y, matrix.get(x, y));
				}
			}
		}
		return scaled;
	}
	
	public int maskedAmount() {
		return maskedAmount;
	}

	public CanvasWithZoom getCanvas() {
		canvasWithZoom.getContentGroup().getChildren().remove(p);
		return canvasWithZoom;
	}

	public CanvasWithZoom addEvent() {
		canvasWithZoom.addEventFilter(MouseEvent.MOUSE_CLICKED, eh);
		return canvasWithZoom;
	}

	public CanvasWithZoom removeEvent() {
		canvasWithZoom.removeEventFilter(MouseEvent.MOUSE_CLICKED, eh);
		return canvasWithZoom;
	}

}
